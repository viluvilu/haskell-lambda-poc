resource "aws_api_gateway_rest_api" "haskell-api-gateway" {
  name        = "Haskell serverless example"
  description = "Terraform Serverless Haskell Example"
  body        = "${data.template_file.api_swagger.rendered}"
}
data "template_file" "api_swagger" {
  template = "${file("${path.module}/swagger.yaml")}"

  vars = {
    lambda_test_post = "${aws_lambda_function.my-haskell-lambda.invoke_arn}"
  }
}

resource "aws_lambda_permission" "api-gateway-invoke-my-haskell-lambda" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.my-haskell-lambda.arn}"
  principal     = "apigateway.amazonaws.com"

  # The /*/* portion grants access from any method on any resource
  # within the specified API Gateway.
  source_arn = "${aws_api_gateway_deployment.api-gateway-deployment.execution_arn}/*/*"
}

resource "aws_api_gateway_deployment" "api-gateway-deployment" {
  rest_api_id = "${aws_api_gateway_rest_api.haskell-api-gateway.id}"
  stage_name  = "dev"
}

output "url" {
  value = "${aws_api_gateway_deployment.api-gateway-deployment.invoke_url}/api"
}
