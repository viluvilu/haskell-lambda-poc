variable "lambda_version" {
  type = "string"
}

variable "resource_prefix" {
  type = "string"
}

resource "aws_s3_bucket" "lambda-bucket" {
  bucket = "${var.resource_prefix}lambda-bucket"
  acl    = "private"

  tags = {
    Environment = "Dev"
  }
}

resource "aws_iam_role" "lambda_exec" {
  name = "${var.resource_prefix}serverless-example-lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "my-haskell-lambda" {
  function_name = "${var.resource_prefix}my-haskell-lambda"
  handler       = "src/Lib.handler"
  role          = "${aws_iam_role.lambda_exec.arn}"
  runtime       = "provided"
  timeout       = 60
  s3_bucket     = "${var.resource_prefix}lambda-bucket"
  s3_key        = "${var.lambda_version}/function.zip"
}