variable "aws_region" {
  type = "string"
}

provider "aws" {
  version = "~> 2.0"
  region  = "${var.aws_region}"
}

terraform {
  backend "s3" {
    encrypt        = true
    bucket         = "haskell-lambda-poc-dev-tfstate"
    region         = "eu-central-1"
    key            = "travel/dev/terraform.tfstate"
    dynamodb_table = "haskell-lambda-poc-dev-tfstate"
  }
}
