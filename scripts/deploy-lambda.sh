#!/usr/bin/env bash
set -euxo pipefail
# build
cd $(git rev-parse --show-toplevel)/my-haskell-lambda
make
version=$(git rev-parse HEAD)
awsudo -u private aws s3 cp "build/function.zip" "s3://haskell-lambda-poc-dev-lambda-bucket/${version}/function.zip"
echo "lambda_version=\"${version}\"" > $(git rev-parse --show-toplevel)/terraform/lambda-version.auto.tfvars
echo "Deploying!"
cd $(git rev-parse --show-toplevel)/terraform
awsudo -u private terraform apply