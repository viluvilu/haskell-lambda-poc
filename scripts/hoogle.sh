#!/usr/bin/env bash
set -euxo pipefail

cd $(git rev-parse --show-toplevel)/my-haskell-lambda
stack hoogle -- generate --local
stack hoogle -- server --local --port=8888