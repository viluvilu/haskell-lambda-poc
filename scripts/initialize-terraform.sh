#!/usr/bin/env bash
set -euxo pipefail

cd $(git rev-parse --show-toplevel)/.terraform-setup
awsudo -u private terraform init
awsudo -u private terraform apply