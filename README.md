# Proof of concept integrating Haskell as Serverless and for Cognito events

[AWS Haskell Runtime](https://github.com/theam/aws-lambda-haskell-runtime)

## Requirements

- [aws cli]()
- [awsudo](https://github.com/makethunder/awsudo) 
- [terraform cli](https://www.terraform.io/docs/cli-index.html)
- [aws profile called "private"](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html)
- [stack](https://docs.haskellstack.org/en/stable/README/)

## Getting started

1. Some AWS resources are named globally, i.e. bucket names must be unique across all existing buckets. This means that you have to change some of the resource names in terraform, so replace "my-haskell-lambda" with something unique across all tf files.
1. Set up your TF state by running: scripts/initialize-terraform.sh 
1. Deploy your lambda by running scripts/deploy-lambda.sh

## Helpful Haskell tooling

[haskell](https://www.haskell.org/) 
[hoogle](https://hoogle.haskell.org) a Haskell API search engine
[haddock](https://www.haskell.org/haddock/) Tool for generating Haskell documentation
[stack](https://docs.haskellstack.org/en/stable/README/) The easiest Haskell build tool
[hpack](https://github.com/sol/hpack) It helps to generate your .cabal (build) file. The cabal format has some idiosyncrasies that are slightly improved by this. This also means, when used, you ALWAYS edit the package.yaml file and not the cabal file.

## Usefull commands

- `stack ghci` Start a repl with your dependencies